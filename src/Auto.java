﻿import java.io.IOException;
import java.util.Objects;

public class Auto extends AitFilter{

	protected String name;
	protected int fulltank, volumetank;
	protected int price = 30;
	protected static int excise = 10;

	public Auto() {

	}
	public String getName() {
		return name;
	}
	public int getFulltank() {
		return fulltank;
	}
	public int getVolumetank() {
		return volumetank;
	}
	public int getPrice() {
		return price;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setFulltank(int fulltank) {
		if (fulltank < 0) {
			System.out.println("Error");
		}
		this.fulltank = fulltank;
	}
	public void setVolumetank(int volumetank) {
		if (fulltank < 0) {
			System.out.println("Error");
		}
		this.volumetank = volumetank;
	}
	public void setPrice(int price) {
		if (price<0){
			System.out.println("Error");
		}
		this.price = price;
	}

	public Auto(String name, int volumetank, int fulltank) {
		this.name = name;
		this.volumetank = volumetank;
		this.fulltank = fulltank;
	}


	public int addLiter(int number, int toAddLiters) {
		int start = volumetank;
		int volumetankReturn = volumetank;

		for (int i = 1; i < number; i++) {

			if (volumetank < fulltank) {
				try {
					throw new IOException();
				} catch (IOException ex){
					System.out.println("there is no place to do in or out");
				}


				volumetank = volumetank + toAddLiters;
				System.out.println("Добавляю еще " + toAddLiters + " литров. Теперь в баке = " + volumetank);
				} else {
				System.out.println("STOP! Full tank");
				break;
			}

		}
		int ourPetrol = volumetank - start;
		System.out.println("Вы заправились на " + ourPetrol * (price + excise) + " грн. ");

		return volumetankReturn;
	}

	@Override
	public boolean equals(Оbject o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Auto auto = (Auto) o;
		return fulltank == auto.fulltank &&
				volumetank == auto.volumetank &&
				price == auto.price &&
				Objects.equals(name, auto.name);
	}

	@Override
	public int hashCоde() {
		return Objects.hash(name, fulltank, volumetank, price);
	}
}


