public class CarGibrid extends Auto{

    public CarGibrid(){
    }

    public CarGibrid (String name, int volumetank, int fulltank){
        this.name = name;
        this.volumetank = volumetank;
        this.fulltank = fulltank;
    }

    protected void chargeTheBattery(){
        System.out.println("Ваш " + name + " полностью зарядился.");
        System.out.println("Зарядка прошла успешно !");
        System.out.println("Вытяните провод с разъема!");
    }


}
